package com.micheledivincenzo.menunearby;


import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

public class RestaurantRequest extends StringRequest {

    private static final String LOGIN_REQUEST_URL1 = "http://micheledivincenzo.esy.es/QueryBrowse.php";
    private static final String LOGIN_REQUEST_URL2 = "http://micheledivincenzo.esy.es/QueryProfileBrowse.php";
    private static final String LOGIN_REQUEST_URL3 = "http://micheledivincenzo.esy.es/QueryGetImage.php";
    private Map<String, String> params;

    public RestaurantRequest(float lat, float lng, Response.Listener<String> listener){
        super(Method.POST, LOGIN_REQUEST_URL1, listener, null);
        params = new HashMap<>();

        params.put("lat", lat+"");
        Log.i("Controllo", lat+"");
        params.put("lng", lng+"");
        Log.i("Controllo", lng+"");
        Log.i("Controllo", "parametri gps passati");
    }
    public RestaurantRequest(float lat, float lng, String nomeutente, Response.Listener<String> listener){
        super(Method.POST, LOGIN_REQUEST_URL2, listener, null);
        params = new HashMap<>();

        params.put("lat", lat+"");
        params.put("lng", lng+"");
        params.put("nomeutente", nomeutente);
        Log.i("Controllo", "parametri gps passati");
    }
    public RestaurantRequest(int id, Response.Listener<String> listener){
        super(Method.POST, LOGIN_REQUEST_URL3, listener, null);
        params = new HashMap<>();

        params.put("id", id+"");
        Log.i("Controllo", "parametri id passati");
    }

    @Override
    public Map<String, String> getParams() {
        Log.i("Controllo", "get parametri");
        return params;
    }

}
