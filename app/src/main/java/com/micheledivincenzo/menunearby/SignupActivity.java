package com.micheledivincenzo.menunearby;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends AppCompatActivity {

    EditText etNomeUtente;
    EditText etPassword;
    EditText etConfermaPassword;
    EditText etEta;
    Button btnSignup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        etNomeUtente = (EditText) findViewById(R.id.etNomeUtente);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfermaPassword = (EditText) findViewById(R.id.etConfermaPassword);
        etEta = (EditText) findViewById(R.id.etEta);
        btnSignup = (Button) findViewById(R.id.btnSignup);

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String nomeutente = etNomeUtente.getText().toString();
                final String password = etPassword.getText().toString();
                final int eta = Integer.parseInt(etEta.getText().toString());

                //Confronto le password
                if(etPassword.getText().toString().equals(etConfermaPassword.getText().toString())) {

                    Response.Listener<String> listener = new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");

                                if (success) {
                                    Toast.makeText(getBaseContext(), "Registrazione effettuata", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getBaseContext(), LoginActivity.class);
                                    startActivity(i);
                                } else {
                                    Snackbar.make(btnSignup, "Nome utente già in uso. Scegline un altro e riprova", Snackbar.LENGTH_LONG).show();
                                    Log.i("controllo", "success = false");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    SignupRequest signupRequest = new SignupRequest(nomeutente, password, eta, listener);
                    RequestQueue coda = Volley.newRequestQueue(getBaseContext());
                    coda.add(signupRequest);

                }
                else{
                    Snackbar.make(getCurrentFocus(), "Le due password non coincidono. Controlla e riprova", Snackbar.LENGTH_LONG).show();
                }
            }
        });


    }
}
