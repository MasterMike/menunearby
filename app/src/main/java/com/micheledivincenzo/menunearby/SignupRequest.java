package com.micheledivincenzo.menunearby;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Miche on 23/09/2016.
 */

public class SignupRequest extends StringRequest{

    private static final String REGISTER_REQUEST_URL = "http://micheledivincenzo.esy.es/Register.php";
    private Map<String, String> params;

    public SignupRequest(String nomeutente, String password, int eta, Response.Listener<String> listener){
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();

        params.put("nomeutente", nomeutente);
        params.put("password", password);
        params.put("eta", eta + "");
        Log.i("controllo", "parametri signup passati");
    }

    @Override
    public Map<String, String> getParams() {
        Log.i("controllo", "get parametri signup");
        return params;
    }
}
