package com.micheledivincenzo.menunearby;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;


public class ImageUploadRequest extends StringRequest {

    private static final String REGISTER_REQUEST_URL = "http://micheledivincenzo.esy.es/Upload.php";
    private Map<String, String> params;

    public ImageUploadRequest(String nomeutente, String foto, float lat, float lng, String nomeristorante, String citta, String indirizzo, Response.Listener<String> listener){
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();

        params.put("nomeutente", nomeutente);
        params.put("foto", foto);
        params.put("lat", lat+"");
        params.put("lng", lng+"");
        params.put("nomeristorante", nomeristorante);
        params.put("citta", citta);
        params.put("indirizzo", indirizzo);

        Log.i("Controllo", "parametri: "+params.toString());
    }

    @Override
    public Map<String, String> getParams() {
        Log.i("Controllo", "get parametri foto");
        return params;
    }
}