package com.micheledivincenzo.menunearby;

public class Item {

    public String nomeRistorante, indirizzo, citta, utente;
    public int id;
    public float distanza;

    public float getDistanza() {
        return distanza;
    }

    public void setDistanza(float distanza) {
        this.distanza = distanza;
    }



    public String getNomeRistorante() {
        return nomeRistorante;
    }

    public void setNomeRistorante(String nomeRistorante) {
        this.nomeRistorante = nomeRistorante;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Item(){}

}
