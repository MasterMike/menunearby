package com.micheledivincenzo.menunearby;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Miche on 23/09/2016.
 */

public class LoginRequest extends StringRequest {

    private static final String LOGIN_REQUEST_URL = "http://micheledivincenzo.esy.es/Login.php";
    private Map<String, String> params;

    public LoginRequest(String nomeutente, String password, Response.Listener<String> listener){
        super(Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();

        params.put("nomeutente", nomeutente);
        params.put("password", password);
        Log.i("controllo", "parametri login passati");
    }

    @Override
    public Map<String, String> getParams() {
        Log.i("controllo", "get parametri login");
        return params;
    }

}
