package com.micheledivincenzo.menunearby;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class UploadDetailActivity extends AppCompatActivity {

    private ImageView imagePreview;
    private EditText etNomeRistorante, etIndirizzo, etCitta;
    private FloatingActionButton fab;
    private SharedPreferences pref;
    private String nomeutente;
    //private Uri imageToUpload;
    private int maxWidth, maxHeight;
    private float lat, lng;
    private Uri file_uri;
    private String encoded_string;
    private Bitmap bitmap;
    private ProgressDialog dialog;

    static final int REQUEST_IMAGE_CAMERA = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_detail);

        //Lancio o Camera o Galleria
        Intent i = getIntent();
        boolean isCamera = i.getBooleanExtra("fromCamera", true);
        if(isCamera){
            pickImageFromCamera();
        }else{
            pickImageFromGallery();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO Aggiungere controlli per i campi di testo vuoti
                if(etNomeRistorante.getText().toString().isEmpty() || etIndirizzo.getText().toString().isEmpty() || etCitta.getText().toString().isEmpty()){
                    Snackbar.make(view,"Assicurati di aver inserito tutti i campi richiesti", Snackbar.LENGTH_LONG).show();
                } else{
                    dialog=new ProgressDialog(UploadDetailActivity.this);
                    dialog.setMessage("Caricamento in corso...");
                    dialog.show();
                    upload();
                }
            }
        });

        //Imposto dimensioni massime per le foto
        maxWidth = 1080;
        maxHeight = 1920;

        pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        nomeutente = pref.getString("nomeutente", "");
        toolbar.setSubtitle(nomeutente);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);

        imagePreview = (ImageView) findViewById(R.id.imagePreview);
        etNomeRistorante = (EditText) findViewById(R.id.etNomeRistorante);
        etIndirizzo = (EditText) findViewById(R.id.etIndirizzo);
        etCitta = (EditText) findViewById(R.id.etCitta);

        //Ottengo permessi per localizzazione
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
            }
            return;
        }else{
            lat = requestLatitude();
            lng = requestLongitude();
            //textViewLatitude.setText(lat+"");
            //textViewLongitude.setText(lng+"");
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 10){
            if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                lat = requestLatitude();
                lng = requestLongitude();
                //textViewLatitude.setText(requestLatitude()+"");
                //textViewLongitude.setText(requestLongitude()+"");
            }
        }
    }
    private float requestLatitude(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double latitude = location.getLatitude();
        return (float) latitude;
    }
    private float requestLongitude(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = location.getLongitude();
        return (float) longitude;
    }

    private void pickImageFromCamera(){
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        getFileUri();
        i.putExtra(MediaStore.EXTRA_OUTPUT, file_uri);
        startActivityForResult(i, REQUEST_IMAGE_CAMERA);
    }
    private void getFileUri() {
        String image_name = "testing1234.jpg";
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                + File.separator + image_name
        );

        file_uri = Uri.fromFile(file);
        Log.i("Controllo", "URI: "+file_uri.toString());
    }

    private void pickImageFromGallery(){
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Scegli immagine da...");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, REQUEST_IMAGE_GALLERY);
    }

    public void hideDialog(){
        if(dialog !=null){
            dialog.dismiss();
            dialog=null;
        }
    }

    private void upload(){

        Response.Listener<String> listener = new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");

                    if (success) {
                        hideDialog();
                        Toast.makeText(getBaseContext(), "Caricamento effettuato", Toast.LENGTH_SHORT).show();
                        Log.i("controllo", "success = true");
                        Intent i = new Intent(getBaseContext(), UserAreaActivity.class);
                        startActivity(i);
                    } else {
                        hideDialog();
                        Toast.makeText(getBaseContext(), "Errore nel caricamento", Toast.LENGTH_SHORT).show();
                        Log.i("controllo", "success = false");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        ImageUploadRequest signupRequest = new ImageUploadRequest(nomeutente, encoded_string, lat, lng, etNomeRistorante.getText().toString(), etCitta.getText().toString(), etIndirizzo.getText().toString(), listener);
        RequestQueue coda = Volley.newRequestQueue(getApplicationContext());
        coda.add(signupRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAMERA&& resultCode == RESULT_OK) {
            imagePreview.setImageURI(file_uri);
            bitmap = ((BitmapDrawable) imagePreview.getDrawable()).getBitmap();
            new Encode_image().execute();
        }
        if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
            file_uri = data.getData();
            imagePreview.setImageURI(file_uri);
            bitmap = ((BitmapDrawable) imagePreview.getDrawable()).getBitmap();
            new Encode_image().execute();
        }
    }

    private class Encode_image extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {


            //bitmap = BitmapFactory.decodeFile(file_uri.getPath());

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            //bitmap = Bitmap.createScaledBitmap(bitmap,1080,1920,true);
            bitmap = scaleBitmap(bitmap);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            //bitmap.recycle();
            Log.i("Controllo", "Creata Bitmap");
            byte[] array = stream.toByteArray();
            Log.i("Controllo", "Creato Array");
            encoded_string = Base64.encodeToString(array, 0);
            Log.i("Controllo", "Creata Stringa");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            imagePreview.setImageBitmap(bitmap);
            Log.i("Controllo", "Impostata immagine");
            //makeRequest();
            //Log.i("Controllo", "Fatta richiesta");
        }
    }
    private Bitmap scaleBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        Log.v("Pictures", "Width and height are " + width + "--" + height);

        if (width > height) {
            // landscape
            float ratio = (float) width / maxWidth;
            width = maxWidth;
            height = (int)(height / ratio);
        } else if (height > width) {
            // portrait
            float ratio = (float) height / maxHeight;
            height = maxHeight;
            width = (int)(width / ratio);
        } else {
            // square
            height = maxHeight;
            width = maxWidth;
        }

        Log.v("Pictures", "after scaling Width and height are " + width + "--" + height);

        bm = Bitmap.createScaledBitmap(bm, width, height, true);
        return bm;
    }

}
