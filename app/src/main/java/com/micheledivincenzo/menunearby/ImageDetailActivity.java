package com.micheledivincenzo.menunearby;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ImageDetailActivity extends AppCompatActivity {

    private ProgressDialog dialog;
    private FloatingActionMenu menuFab;
    private FloatingActionButton btnElimina, btnSegnala, btnNaviga;
    private SharedPreferences pref;

    private ImageView imageMenu;
    private TextView tvNomeRistorante, tvIndirizzo, tvCitta, tvNomeUtente;
    private int id;
    private float lat, lng;
    private String strNomeRistorante, strIndirizzo, strCitta, strNomeUtenteImmagine, strNomeUtenteAttuale;
    private Bitmap bitmap;
    private String encoded_string;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dialog=new ProgressDialog(this);
        dialog.setMessage("Caricamento...");
        dialog.show();

        //Imposto Floating Action Button..
        menuFab = (FloatingActionMenu)findViewById(R.id.menu_fab);
        btnElimina = (FloatingActionButton)findViewById(R.id.btnElimina);
        btnElimina.setVisibility(View.GONE);
        btnSegnala = (FloatingActionButton)findViewById(R.id.btnSegnala);
        btnSegnala.setVisibility(View.GONE);
        btnNaviga = (FloatingActionButton)findViewById(R.id.btnNaviga);
        //...e i suoi listener
        btnNaviga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuFab.close(true);
                //Apro google maps per indicazioni ristorante
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr="+lat+","+lng));
                startActivity(intent);
            }
        });
        btnElimina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST,"http://micheledivincenzo.esy.es/QueryDeleteImage.php", new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "Contenuto eliminato", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(getApplicationContext(), UserAreaActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    protected Map<String,String> getParams(){
                        Map<String,String> params = new HashMap<String, String>();

                        params.put("id", id+"");
                        Log.i("Controllo", "ID da eliminare: "+params.toString());

                        return params;
                    }
                };
                Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
            }
        });

        imageMenu = (ImageView) findViewById(R.id.imageViewMenu);
        tvNomeRistorante = (TextView) findViewById(R.id.tvNomeRistorante);
        tvIndirizzo = (TextView) findViewById(R.id.tvIndirizzo);
        tvCitta = (TextView) findViewById(R.id.tvCitta);
        tvNomeUtente = (TextView) findViewById(R.id.tvNomeUtente);

        //Ottengo l'ID dell'oggetto dall'intent e ottengo le stringhe
        Intent i = getIntent();
        id = i.getIntExtra("id", -1);
        strNomeRistorante = i.getStringExtra("nomeristorante");
        strIndirizzo = i.getStringExtra("indirizzo");
        strCitta = i.getStringExtra("citta");
        strNomeUtenteImmagine = i.getStringExtra("utente");

        tvNomeRistorante.setText(strNomeRistorante);
        tvIndirizzo.setText(strIndirizzo);
        tvCitta.setText(strCitta);
        tvNomeUtente.setText("Caricato da "+strNomeUtenteImmagine);

        //Ottengo nome utente attuale
        pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        strNomeUtenteAttuale = pref.getString("nomeutente", "");

        Log.i("Controllo",strNomeUtenteAttuale);

        //Imposta visibilità tasto elimina se l'utente è quello che ha caricato l'immagine
        if(strNomeUtenteAttuale.equalsIgnoreCase(strNomeUtenteImmagine)){
            btnElimina.setVisibility(View.VISIBLE);
        }

        //Carico l'immagine dal server nella imageview
        impostaImmagineServer();


    }

    private void impostaImmagineServer(){
        Response.Listener<String> listener = new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {

                Log.i("Controllo", "JSON ricevuto: "+response);
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for(int i=0;i<jsonArray.length();i++) {
                    try{
                        JSONObject obj=jsonArray.getJSONObject(i);

                        encoded_string = obj.getString("foto");
                        lat = (float) obj.getDouble("lat");
                        lng = (float) obj.getDouble("lng");

                    }catch(JSONException ex){
                        ex.printStackTrace();
                    }
                }

                //Decodifico l'immagine ottenuta
                byte[] decodedString = Base64.decode(encoded_string, Base64.DEFAULT);
                bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                imageMenu.setImageBitmap(bitmap);
                hideDialog();
            }
        };
        Log.i("controllo", "richiesta");
        RestaurantRequest restaurantRequest = new RestaurantRequest(id, listener);
        RequestQueue coda = Volley.newRequestQueue(getApplicationContext());
        coda.add(restaurantRequest);
    }

    public void hideDialog(){
        if(dialog !=null){
            dialog.dismiss();
            dialog=null;
        }
    }

}
