package com.micheledivincenzo.menunearby;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;


public class Adapter extends BaseAdapter{
    private LayoutInflater inflater;
    private Activity activity;
    private List<Item> items;

    public Adapter(Activity activity,List<Item> items){
        this.activity=activity;
        this.items=items;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater==null){
            inflater=(LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView ==null){
            convertView=inflater.inflate(R.layout.row_layout,null);
        }
        TextView tvNomeRistorante= (TextView) convertView.findViewById(R.id.tvNomeRistorante);
        TextView tvIndirizzo= (TextView) convertView.findViewById(R.id.tvIndirizzo);
        TextView tvCitta= (TextView) convertView.findViewById(R.id.tvCitta);
        TextView tvUtente= (TextView) convertView.findViewById(R.id.tvNomeUtente);
        TextView tvDistanza= (TextView) convertView.findViewById(R.id.tvDistanza);

        //Ottengo dati per le righe
        Item item=items.get(position);
        //Imposto i testi
        tvNomeRistorante.setText(item.getNomeRistorante());
        tvIndirizzo.setText(item.getIndirizzo());
        tvCitta.setText(item.getCitta());
        tvUtente.setText(item.getUtente());
        float tronc = (float) (Math.floor(item.getDistanza() * 1000.0) / 1000.0);
        tvDistanza.setText(tronc+" km");

        return convertView;
    }
}
