package com.micheledivincenzo.menunearby;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class NearbyFragment extends Fragment {

    private ProgressDialog dialog;
    private List<Item> array = new ArrayList<Item>();
    private ListView listView;
    private Adapter adapter;
    private float lat, lng;


    public NearbyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nearby, container, false);
        setHasOptionsMenu(true);


        listView = (ListView) view.findViewById(R.id.listView);
        adapter=new Adapter(getActivity(),array);
        listView.setAdapter(adapter);

        dialog=new ProgressDialog(getContext());
        dialog.setMessage("Caricamento elementi...");
        dialog.show();

        //Carica la lista dei ristoranti disponibili
        showRestaurantList();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item item = (Item) parent.getItemAtPosition(position);

                String nomeristorante, indirizzo, citta, utente;
                nomeristorante = item.getNomeRistorante();
                indirizzo = item.getIndirizzo();
                citta = item.getCitta();
                utente = item.getUtente();

                Intent i = new Intent (getActivity().getApplicationContext(), ImageDetailActivity.class);
                i.putExtra("id", item.getId());
                i.putExtra("nomeristorante", nomeristorante);
                i.putExtra("indirizzo", indirizzo);
                i.putExtra("citta", citta);
                i.putExtra("utente", utente);
                startActivity(i);
            }
        });

        return view;
    }

    private void showRestaurantList(){

        //Ottengo permessi per localizzazione
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET      }, 10);
            }
            return;
        }else{
            lat = requestLatitude();
            lng = requestLongitude();
            //textViewLatitude.setText(lat+"");
            //textViewLongitude.setText(lng+"");
        }

        if(!array.isEmpty()){
            array.clear();
        }

        Response.Listener<String> listener = new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                hideDialog();
                Log.i("Controllo", "JSON ricevuto: "+response);
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for(int i=0;i<jsonArray.length();i++) {
                    try{
                        JSONObject obj=jsonArray.getJSONObject(i);
                        Item item=new Item();
                        item.setNomeRistorante(obj.getString("nomeristorante"));
                        item.setIndirizzo(obj.getString("indirizzo"));
                        item.setCitta(obj.getString("citta"));
                        item.setUtente(obj.getString("utente"));
                        item.setId(obj.getInt("id"));
                        item.setDistanza((float)obj.getDouble("distance"));

                        //add to array
                        array.add(item);
                    }catch(JSONException ex){
                        ex.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();

            }
        };
        Log.i("controllo", "richiesta");
        RestaurantRequest restaurantRequest = new RestaurantRequest(lat, lng, listener);
        RequestQueue coda = Volley.newRequestQueue(getContext());
        coda.add(restaurantRequest);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 10){
            if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                lat = requestLatitude();
                lng = requestLongitude();
                //textViewLatitude.setText(requestLatitude()+"");
                //textViewLongitude.setText(requestLongitude()+"");
            }
        }
    }

    private float requestLatitude(){
        LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double latitude = location.getLatitude();
        return (float) latitude;
    }
    private float requestLongitude(){
        LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = location.getLongitude();
        return (float) longitude;
    }

    public void hideDialog(){
        if(dialog !=null){
            dialog.dismiss();
            dialog=null;
        }
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.nearby_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            dialog=new ProgressDialog(getContext());
            dialog.setMessage("Caricamento elementi...");
            dialog.show();
            showRestaurantList();
        }
        return super.onOptionsItemSelected(item);
    }
}
