package com.micheledivincenzo.menunearby;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashActivity extends AppCompatActivity {

    ImageView splashLogo;

    SharedPreferences pref;
    SharedPreferences.Editor prefEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashLogo = (ImageView) findViewById(R.id.splash_logo);

        pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        prefEdit = pref.edit();

        if(pref.getString("nomeutente", "").equals("") && pref.getString("password", "").equals("")){
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);
            finish();
        }
        else{
            doLogin(pref.getString("nomeutente", ""), pref.getString("password",""));
        }

        Log.i("controllo", "Credenziali salvate:");
        Log.i("controllo", pref.getString("nomeutente", ""));
        Log.i("controllo", pref.getString("password", ""));
    }

    private void doLogin(String n, String p){
        final String nomeutente=n;
        final String password=p;

        Response.Listener<String> listener = new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    Log.i("controllo", "success = " + success);

                    if(success){
                        //String nomeutente = jsonResponse.getString("nomeutente");
                        Log.i("controllo", "Accesso effettuato");
                        int eta = jsonResponse.getInt("eta");

                        Intent i = new Intent (getBaseContext(), UserAreaActivity.class);
                        i.putExtra("nomeutente", nomeutente);
                        i.putExtra("eta", eta);

                        //Salvo la sessione dentro le SharedPreferences
                        prefEdit.putString("nomeutente", nomeutente);
                        prefEdit.putString("password", password);
                        prefEdit.apply();
                        Log.i("controllo", pref.getString("nomeutente", ""));
                        Log.i("controllo", pref.getString("password", ""));

                        startActivity(i);
                        finish();
                    }
                    else{
                        Snackbar.make(splashLogo, "Accesso non effettuato. Controlla nome utente e password e riprova", Snackbar.LENGTH_LONG).show();
                        prefEdit.clear();
                        Log.i("controllo", "success = false");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        Log.i("controllo", "richiesta");
        LoginRequest loginRequest = new LoginRequest(nomeutente, password, listener);
        RequestQueue coda = Volley.newRequestQueue(getBaseContext());
        coda.add(loginRequest);
    }
}
