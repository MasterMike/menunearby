package com.micheledivincenzo.menunearby;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserAreaActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //TextView txWelcome;
    Toolbar toolbar;
    FloatingActionMenu menuFab;
    FloatingActionButton btnGalleryUpload, btnCameraUpload;
    SharedPreferences pref;
    SharedPreferences.Editor prefEdit;

    private String mCurrentPhotoPath;
    static final int REQUEST_IMAGE_CAMERA = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        prefEdit = pref.edit();

        //Inizializzo Fragment principale
        NearbyFragment nearbyFragment = new NearbyFragment();
        //MyProfileFragment myProfileFragment = new MyProfileFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_user_area, nearbyFragment).commit();
        ////////////////////////////////////////////////////

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Floating Action Button originale
        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */


        //Floating Action Button alternativo
        menuFab = (FloatingActionMenu)findViewById(R.id.menu_fab);
        btnCameraUpload = (FloatingActionButton)findViewById(R.id.btnCameraUpload);
        btnGalleryUpload = (FloatingActionButton)findViewById(R.id.btnGalleryUpload);

        btnCameraUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Snackbar.make(v,"Scelto Upload da fotocamera", Snackbar.LENGTH_LONG).show();
                lanciaActivityPerCamera();
                menuFab.close(true);
            }
        });

        btnGalleryUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Snackbar.make(v,"Scelto Upload da galleria", Snackbar.LENGTH_LONG).show();
                lanciaActivityPerGalleria();
                menuFab.close(true);
            }
        });


        //Inizializzo messaggio per l'utente
        //txWelcome = (TextView) findViewById(R.id.txBenvenuto);


        //Intent i = getIntent();
        //String nomeutente = i.getStringExtra("nomeutente");
        //int eta = i.getIntExtra("eta", -1);

        //Ottengo sessione da SharedPreferences
        String nomeutente = pref.getString("nomeutente", "");
        int eta = pref.getInt("eta", -1);

        //txWelcome.setText("Benvenuto, " + nomeutente + " - " + eta);
        toolbar.setSubtitle(nomeutente + " • " + eta);

        //Inizializzo Navigation Drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



    }
/*
    private void pickImageFromCamera(){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.i("Controllo", "IOException");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAMERA);
            }
        }
    }

    private void pickImageFromGallery(){
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Scegli immagine da...");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, REQUEST_IMAGE_GALLERY);
    }

  */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == REQUEST_IMAGE_CAMERA || requestCode == REQUEST_IMAGE_GALLERY) && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            Intent i = new Intent (getApplicationContext(), UploadDetailActivity.class);
            i.setData(selectedImage);
            startActivity(i);
            //imageView.setImageURI(selectedImage);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = Environment.getExternalStoragePublicDirectory(
        //      Environment.DIRECTORY_PICTURES);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void lanciaActivityPerCamera(){
        Intent i = new Intent(getApplicationContext(), UploadDetailActivity.class);
        i.putExtra("fromCamera", true);
        startActivity(i);
    }

    private void lanciaActivityPerGalleria(){
        Intent i = new Intent(getApplicationContext(), UploadDetailActivity.class);
        i.putExtra("fromCamera", false);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            Intent i = new Intent(getBaseContext(),LoginActivity.class);
            prefEdit.clear();
            prefEdit.apply();
            startActivity(i);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_aroundMe) {
            NearbyFragment nearbyFragment = new NearbyFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_user_area, nearbyFragment).commit();
            toolbar.setTitle(R.string.title_nearby);
            //Toast.makeText(getBaseContext(), "Fragment 1", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_profile) {
            MyProfileFragment myProfileFragment = new MyProfileFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_user_area, myProfileFragment).commit();
            toolbar.setTitle(R.string.title_profile);
            //Toast.makeText(getBaseContext(), "Fragment 2", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_logout) {
            Intent i = new Intent(getBaseContext(),LoginActivity.class);
            prefEdit.clear();
            prefEdit.apply();
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
